terraform {
  required_version = ">= 0.12.26"
  backend "gcs" {
    credentials = "serviceaccount.json"
    bucket = "tfstate-demo-appsb"
  }
}


provider "google" {
  version = "~> 3.43.0"
  project = var.project
  region  = var.region
  credentials = file("serviceaccount.json")
}

provider "google-beta" {
  version = "~> 3.43.0"
  project = var.project
  region  = var.region
  credentials = file("serviceaccount.json")
}

# data "google_client_config" "default" {}

# data "google_container_cluster" "my_cluster" {
#   name     = data.terraform_remote_state.gke.outputs.kubernetes_cluster_name
#   location = data.terraform_remote_state.gke.outputs.region
# }
module "gke_cluster" {

  source = "../modules/gke-cluster"
  name = var.cluster_name
  project  = var.project
  location = var.location
  network  = module.vpc_network.network

  # We're deploying the cluster in the 'public' subnetwork to allow outbound internet access
  # See the network access tier table for full details:
  # https://github.com/gruntwork-io/terraform-google-network/tree/master/modules/vpc-network#access-tier
  subnetwork = module.vpc_network.private_subnetwork

  # When creating a private cluster, the 'master_ipv4_cidr_block' has to be defined and the size must be /28
  master_ipv4_cidr_block = var.master_ipv4_cidr_block

  # This setting will make the cluster private
  enable_private_nodes = "false"

  # To make testing easier, we keep the public endpoint available. In production, we highly recommend restricting access to only within the network boundary, requiring your users to use a bastion host or VPN.
  disable_public_endpoint = "false"

  # With a private cluster, it is highly recommended to restrict access to the cluster master
  # However, for testing purposes we will allow all inbound traffic.
  master_authorized_networks_config = [
    {
      cidr_blocks = [
        {
          cidr_block   = "0.0.0.0/0"
          display_name = "all-for-testing"
        },
      ]
    },
  ]

  cluster_secondary_range_name = module.vpc_network.private_subnetwork_secondary_range_name

  enable_vertical_pod_autoscaling = var.enable_vertical_pod_autoscaling

  resource_labels = {
    environment = "testing"
  }
}



resource "google_container_node_pool" "node_pool" {
  provider = google-beta

  name     = "private-pool"
  project  = var.project
  location = var.location
  cluster  = module.gke_cluster.name

  initial_node_count = "1"

  autoscaling {
    min_node_count = "3"
    max_node_count = "5"
  }

  management {
    auto_repair  = "true"
    auto_upgrade = "true"
  }

  node_config {
    image_type   = "COS"
    machine_type = "n1-standard-1"

    labels = {
      private-pool-demo = "false"
    }

    # Add a private tag to the instances. See the network access tier table for full details:
    # https://github.com/gruntwork-io/terraform-google-network/tree/master/modules/vpc-network#access-tier
    tags = [
      module.vpc_network.private,
      "private-pool-demo",
    ]

    disk_size_gb = "30"
    disk_type    = "pd-standard"
    preemptible  = false

    service_account = module.gke_service_account.email

    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }

  lifecycle {
    ignore_changes = [initial_node_count]
  }

  timeouts {
    create = "30m"
    update = "30m"
    delete = "30m"
  }
}
provider "kubernetes" {
    version = "~> 1.3"
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE A CUSTOM SERVICE ACCOUNT TO USE WITH THE GKE CLUSTER
# ---------------------------------------------------------------------------------------------------------------------

module "gke_service_account" {

  source = "../modules/gke-service-account"

  name        = var.cluster_service_account_name
  project     = var.project
  description = var.cluster_service_account_description
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE A NETWORK TO DEPLOY THE CLUSTER TO
# ---------------------------------------------------------------------------------------------------------------------

module "vpc_network" {
  source = "../modules/vpc-network"

  name_prefix = "${var.cluster_name}-network-${random_string.suffix.result}"
  project     = var.project
  region      = var.region

  cidr_block           = var.vpc_cidr_block
  #secondary_cidr_block = var.vpc_secondary_cidr_block
}

# Use a random suffix to prevent overlap in network names
resource "random_string" "suffix" {
  length  = 4
  special = false
  upper   = false
}

module "gcs_bucket" {
    source = "../modules/gcs-bucket"
    bucket_name = "tfstate-demoo-appsb"
    location = "us-central1"
    name_suffix = "dev"
}
# provisioner "local-exec" {
#     command = "gcloud container clusters get-credentials ${module.gke-cluster.name} --zone  ${module.gke-cluster.gke-cluster.zone} --project ${module.gke-cluster.gke-cluster.project}"
# }
# resource "kubernetes_service_account" "ansible" {
#   depends_on = [google_container_node_pool.node_pool]
#   metadata {
#   name      = "ansible-sa"
#   namespace = "kube-system"
#   }
#   automount_service_account_token = true
# }

# resource "kubernetes_cluster_role_binding" "ansible" {
#   depends_on = [google_container_node_pool.node_pool]
#   metadata {
#     name = "ansible-sa"
#   }
#   role_ref {
#     kind      = "ClusterRole"
#     name      = "cluster-admin"
#     api_group = "rbac.authorization.k8s.io"
#   }
#   subject {
#     kind = "ServiceAccount"
#     name = "ansible-sa"
#     namespace = "kube-system"
#   }
# }
# module "gke-auth" {
#    source = "../modules/gke-auth"
#    location = var.location
#    project_id = var.project
#    cluster_name = var.cluster_name
# }
#module "k8s-service-account" {
 #   source = "../modules/k8s-service-account"
  #  name = "ansible"
   # namespace = "default"
#}
