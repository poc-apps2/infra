#!/bin/bash

gsutil mb -p learning-project-300210 -c NEARLINE -l US-CENTRAL1 -b on gs://tfstate-demo-appsb
gcloud iam service-accounts create terraform --description="terraform service account" --display-name="terraform"
gcloud projects add-iam-policy-binding learning-project-300210 --member="serviceAccount:terraform@learning-project-300210.iam.gserviceaccount.com" --role="roles/datastore.owner"