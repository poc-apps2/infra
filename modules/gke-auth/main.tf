locals {
  cluster_ca_certificate = data.google_container_cluster.gke_cluster.master_auth != null ? data.google_container_cluster.gke_cluster.master_auth[0].cluster_ca_certificate : ""
  endpoint               = data.google_container_cluster.gke_cluster.endpoint != null ? data.google_container_cluster.gke_cluster.endpoint : ""
  host                   = data.google_container_cluster.gke_cluster.endpoint != null ? "https://${data.google_container_cluster.gke_cluster.endpoint}" : ""
  context                = data.google_container_cluster.gke_cluster.name != null ? data.google_container_cluster.gke_cluster.name : ""
}

data "google_container_cluster" "gke_cluster" {
  name     = var.cluster_name
  location = var.location
  project  = var.project_id
}

data "google_client_config" "provider" {}

resource "local_file" "kubeconfig" {
  content = data.template_file.kubeconfig.rendered
  filename = "..modules/gke-auth/kubeconfig-template.yaml.tpl"
}

data "template_file" "kubeconfig" {
  template = file("..modules/gke-auth//kubeconfig-template.yaml.tpl")

  vars = {
    context                = local.context
    cluster_ca_certificate = local.cluster_ca_certificate
    endpoint               = local.endpoint
    token                  = data.google_client_config.provider.access_token
  }
}